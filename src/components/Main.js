import React from 'react';
import DevTools from 'mobx-react-devtools';

export default ({ children }) => {
  return (
    <div className="App">
      <header className="App-header">
        <h1>CollectHelp</h1>
      </header>
      <div>{children}</div>
      <DevTools />
    </div>
  );
};
