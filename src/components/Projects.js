import React, { Component } from 'react';
import { observer } from 'mobx-react';
import Button from 'react-bootstrap/lib/Button';
import { Link } from 'react-router-dom';

const ProjectsList = ({ projects }) => {
  console.log(projects);
  return (<ul className="list-group">
    {projects.map(project =>
        <li className="list-group-item projects-list-item" key={project.id}>
            <img src={'http://placehold.it/150x150'} alt="..." className="img-thumbnail projects-list-item-image" />
            <div className="projects-list-item-about">
              <div className="float-right">{project.title}</div>
              <div className="float-right">{project.description}</div>
              <div>{project.resourcesNeeded.map(resource => <span className="btn btn-primary projects-list-item-resource" key={resource.id}>{resource.title} {resource.amount}</span>)}</div>
            </div>
        </li>)}
  </ul>);
}

@observer
export default class Projects extends Component {
  componentWillMount() {
    console.log('wiil moutn', this.props);
    this.props.store.loadProjects();
  }

  render() {
    const { projects, someText } = this.props.store;
    console.log('projects', projects.toJS());
    return (<div className="text-left">
        <Link to='/projects/create'>
          <Button>Создать проект {someText}</Button>
        </Link>
        <ProjectsList projects={projects} />
    </div>);
  }
}
