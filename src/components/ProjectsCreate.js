import React, { Component } from 'react';
import FormBuilder from '../utils/formBuilder';
import Button from 'react-bootstrap/lib/Button';
import { observer } from 'mobx-react';

class Resources extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      items: [],
      amount: 0
    };
  }

  add(rows) {
    const { onChange } = this.props;
    this.setState(({ items, title, amount }) => {
      return { items: [ ...items, { id: -(items.length + 1) , title, amount } ], title: '', amount: 0 };
    }, () => onChange(this.state.items));
  }

  delete(id) {
    this.setState(({ items }) => {
      return { items: items.filter(item => item.id !== id) };
    })
  }

  changeText(e) {
    const title = e.target.value;
    this.setState({ title });
  }

  changePrice(e) {
    const amount = e.target.value;
    this.setState({ amount });
  }

  render() {
    const { items, title, price } = this.state;
    return (
      <ul>
        <Button onClick={this.add.bind(this)}>Add</Button>
        <input type="text" value={title} onChange={this.changeText.bind(this)} />
        <input type="number" value={price} onChange={this.changePrice.bind(this)} />
        {items.map(item => <li key={item.id} onClick={() => this.delete(item.id)}>{item.title}</li>)}
      </ul>
    );
  }
}

class ProjectsCreateFormView extends Component {
  render () {
    const{ submit, setInputValue, values, setFormValue } = this.props;
    return (
        <form className="text-left project-create-form" onSubmit={submit}>
          <div className="form-group">
            <label>Название проекта</label>
            <input className="form-control" name="title" onChange={setInputValue} />
          </div>
          <div className="form-group">
            <label>Описание проекта</label>
            <input className="form-control" name="description" onChange={setInputValue} />
          </div>
          <div className="form-group">
            <Resources onChange={value => setFormValue('resources', value)} />
          </div>
          <input type="submit" className="btn" value="save" onChange={setInputValue} />
        </form>
    );
  }
};

@observer
export default class ProjectsCreate extends Component {
  render() {
    const { values, store, history } = this.props;
    console.log(store.create);
    const submitAndRedirect = (fields) => {
      store.create(fields);
      history.push('/projects');
    }
    return (<FormBuilder
      FormComponent={ProjectsCreateFormView}
      formValues={values}
      submit={submitAndRedirect}
    />);
  }
}
