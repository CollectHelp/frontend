import React from 'react';
import {
  Switch,
  Route,
} from 'react-router';

import {
  BrowserRouter as Router,
} from 'react-router-dom';

import Main from './components/Main';
import Projects from './components/Projects';
import ProjectsCreate from './components/ProjectsCreate';
import ProjectsStore from './stores/ProjectsStore';


export default ({ children }) => (<Main>
  <Router>
    <Switch>
      <Route exact path={'/'} render={() => 'hello'} />
      <Route exact path={'/projects'} render={() => <Projects store={ProjectsStore} />} />
      <Route exact path={'/projects/create'} render={props => <ProjectsCreate store={ProjectsStore} {...props} />} />
    </Switch>
  </Router>
</Main>);
