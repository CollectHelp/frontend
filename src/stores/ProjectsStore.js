import { observable, action } from 'mobx';

class Project {
  @observable id;
  @observable title;
  @observable description;
  @observable resourcesNeeded = [];

  constructor({ id, title, description, resourcesNeeded = [] }) {
    this.id = id;
    this.title = title;
    this.description = description;
    resourcesNeeded.forEach(resource => {
      console.log('Project-resource', resource, this.resourcesNeeded.toJS());
      this.resourcesNeeded.push(resource);
      console.log('Project-resource-after', this.resourcesNeeded.toJS());
    });
  }
}

class ResourceNeeded {
  @observable id;
  @observable title;
  @observable amount;

  constructor({ id, title, amount }) {
    this.id = id;
    this.title = title;
    this.amount = amount;
  }
}

class ProjectsStore {
  @observable projects = [];
  @observable isInited = false;
  @observable currentProjectId;
  @observable someText = 'text';

  @action loadProjects() {
    if (!this.isInited) {
      const projects = [
        { id: 1, title: 'amazin project11', resourcesNeeded: [] },
        { id: 2, title: 'amazin project2', resourcesNeeded: [] }
      ];
      projects.map(project => this.projects.push(new Project(project)));
      this.isInited = true;
    }
  }

  @action create({ title, description, resources }) {
    const id = -(this.projects.length + 1);
    const resourcesNeeded = resources.map(fields => new ResourceNeeded(fields));
    this.projects.push(new Project({ title, description, id, resourcesNeeded }));
  }
}

export default new ProjectsStore();
