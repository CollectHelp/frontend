import React, { Component } from 'react';
import PropTypes from 'prop-types';

export function FormErrors(errors) {
  this.name = 'FormErrors';
  this.message = 'Form Error';
  this.errors = errors;
}

FormErrors.prototype = Error.prototype;

export default class FormBuilder extends Component {

  constructor(props) {
    super(props);
    const { formValues, defautlFormValues, externalErrors, externalMessages } = props;
    this.defautlFormValues = props.defautlFormValues;

    const currentValues = (Object.keys(formValues).length > 0) ? formValues : defautlFormValues;

    this.state = {
      formValues: currentValues,
      formErrors: externalErrors,
      formMessages: externalMessages,
    };

    this.setFormValue = this.setFormValue.bind(this);
    this.setInputValue = this.setInputValue.bind(this);
    this.setFormValues = this.setFormValues.bind(this);
    this.processSubmit = this.processSubmit.bind(this);
  }

  componentWillReceiveProps(newProps) {
    const { externalErrors, externalMessages, formValues } = newProps;
    this.setState({
      formErrors: externalErrors,
      formMessages: externalMessages,
    });
    if (Object.keys(formValues).length > 0) {
      this.setFormValues(formValues);
    }
  }

  setFormValue(name, value) {
    const { onTypeValidator } = this.props;
    const formValues = { ...this.state.formValues, ...{ [name]: value } };
    const formErrors = onTypeValidator(formValues) || {};
    this.setState({ formValues, formErrors });
  }

  setInputValue(e) {
    this.setFormValue(e.target.name, e.target.value);
  }


  setFormValues(formValues) {
    this.setState({ formValues });
  }

  processSubmit(e) {
    const { submit, onSubmitValidator } = this.props;
    const { formValues } = this.state;

    if (typeof e.preventDefault === 'function') {
      e.preventDefault();
    }

    const formErrors = onSubmitValidator(formValues) || {};
    this.setState({ formErrors });

    if (Object.keys(formErrors).length === 0) {
      try {
        submit(formValues);
      } catch (err) {
        if (err instanceof FormErrors) {
          const submitErrors = err.errors;
          this.setState({ formErrors: submitErrors });
        } else {
          throw err;
        }
      }
    }
  }

  render() {
    const { FormComponent, formComponentProps } = this.props;
    const { setFormValue, setInputValue } = this;
    const { formErrors, formValues, formMessages } = this.state;

    return (
      <div>
        <FormComponent
          {...formComponentProps}
          setFormValue={setFormValue}
          setInputValue={setInputValue}
          errors={formErrors}
          messages={formMessages}
          values={formValues}
          submit={this.processSubmit}
        />
      </div>
    );
  }
}

FormBuilder.propTypes = {
  FormComponent: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func
  ]).isRequired,
  formComponentProps: PropTypes.object,
  defautlFormValues: PropTypes.object,
  // TODO This row is really requred?
  formValues: PropTypes.object,
  onTypeValidator: PropTypes.func,
  externalErrors: PropTypes.object,
  externalMessages: PropTypes.object,
  submit: PropTypes.func.isRequired
};


FormBuilder.defaultProps = {
  defautlFormValues: {},
  formComponentProps: {},
  onTypeValidator: () => null,
  onSubmitValidator: () => null,
  externalErrors: {},
  externalMessages: {},
  formValues: {}
};
